This is the translation for A.O.Z Reboot Gundam Inle.

The pdf document is located [here](https://tosiaki.gitlab.io/A.O.Z_RE-BOOT_GUNDAM_INLE_VOL1/main.pdf).

If you wish to edit and never used git before, the main document to edit is main.tex. To create a local copy, you can use [github desktop](https://desktop.github.com/) and skip the step to login to github and input the https link from the clone menu (in this case, https://gitlab.com/tosiaki/A.O.Z_RE-BOOT_GUNDAM_INLE_VOL1.git) when asked to input the repository. Github desktop will automatically detect local changes. The CI/CD pipeline can take about 3 minutes to update the pdf.

For viewing and editing TeX documents, you can try [TeXstudio](https://www.texstudio.org/) with [MiKTeX](https://miktex.org/) to get started. You will also need to change one setting in Options→Configure TeXstudio→Build→Default Compiler and set it to XeLaTeX.
