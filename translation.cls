\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{translation}[2020/07/17 Translations Document Class]

\LoadClass{article}

\RequirePackage{fontspec}
\RequirePackage{newunicodechar}
\RequirePackage{xeCJK}
\RequirePackage{ruby}
\RequirePackage[margin=0.1in]{geometry}
\RequirePackage[table]{xcolor}
\RequirePackage{xltabular}
\RequirePackage{booktabs}
\RequirePackage{hyperref}

\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=blue,
    urlcolor=black
}

\setcounter{secnumdepth}{0}

\setCJKmainfont{meiryo.ttc}
\newfontfamily\meiryo{meiryo.ttc}
\newfontfamily\greekfont{OldStandard-Regular.otf}

\renewcommand{\rubysize}{0.7}

\def\arraystretch{1.5}

\newunicodechar{♥}{{\meiryo♥}}
\newunicodechar{♡}{{\meiryo♡}}
\newunicodechar{★}{{\meiryo★}}
\newunicodechar{☆}{{\meiryo☆}}
\newunicodechar{●}{{\meiryo●}}
\newunicodechar{―}{{\meiryo―}}
\newunicodechar{①}{{\meiryo①}}
\newunicodechar{②}{{\meiryo②}}
\newunicodechar{③}{{\meiryo③}}
\newunicodechar{④}{{\meiryo④}}
\newunicodechar{Ⅱ}{{\meiryoⅡ}}
\newunicodechar{Ⅲ}{{\meiryoⅢ}}
\newunicodechar{μ}{{\greekfont μ}}

\makeatletter
\renewcommand{\@maketitle}{\begin{center}{\LARGE \@title \par}\end{center}}
\makeatother

\newcommand\invisiblesection[1]{%
  \refstepcounter{section}%
  \addcontentsline{toc}{section}{#1}}

\newcommand\invisiblesubsection[1]{%
  \refstepcounter{subsection}%
  \addcontentsline{toc}{subsection}{#1}}
